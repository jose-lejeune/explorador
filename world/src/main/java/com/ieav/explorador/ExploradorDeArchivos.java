/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ieav.explorador;

import java.io.File;

/**
 * Explorador de Archivos desarrollado en Java para equipo con Linux y Usuario ubuntu  
 * @author 	Jose Maria Lejeune
 * @version 1.0.0
 */
public class ExploradorDeArchivos {
      public static void main(String[] args) throws ClassNotFoundException {
         
         try{
             String CarpetaActual = "/home/ubuntu";
             String CarpetaSuperior = "/home/ubuntu";
             FormularioExploradorArchivos MiExploradorDeArchivos = new FormularioExploradorArchivos();
             switch(args.length){
                 case 0:
                   MiExploradorDeArchivos.CarpetaSuperior(CarpetaSuperior);
                   MiExploradorDeArchivos.CarpetaAbrir(CarpetaActual);
                   break;
                 case 1:
                   MiExploradorDeArchivos.CarpetaSuperior(CarpetaSuperior);
                   MiExploradorDeArchivos.CarpetaAbrir(args[0]);
                   break;
                 default:
                   MiExploradorDeArchivos.CarpetaSuperior(args[1]);
                   MiExploradorDeArchivos.CarpetaAbrir(args[0]);  
                   break;
             }
             MiExploradorDeArchivos.setVisible(true);   
         } catch(Exception e){
             System.err.println("Error: " + e.getMessage());
         }
         
     }
     
}
